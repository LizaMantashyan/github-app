class Route {
	params;

	constructor(app, config) {
		this.url = config.url || '';
		this.name = config.name || '';
		this.module = config.module;
		this.container = config.container;

		this.app = app;

	}

	run() {
		this.app.activeModule = (new this.module(this.app, this.container, this.params));
		this.app.activeRoute = this;
		this.app.routeChanged(this);
	}

}