class App {
	activeModule = null;
	activeRoute = {};
	listeners = {
		'routeChange': []
	};

	constructor() {
		this.assignEventListeners();
	}	

	start(mainModule) {
		mainModule.render();
		this.onPopState();
	}

	setRoutes(routes) {
		this.routes = routes.map(item => {
			return new Route(this, item);
		});
	}

	assignEventListeners() {
		window.onpopstate = this.onPopState.bind(this);
	}

	onPopState() {
		let search = window.location.search.replace(/^\?/, '');

		let params = {};
		let route = this.routes.find(r => {
			let routeMatches = Array.from(r.url.matchAll(/{(\w*)}/g));
			let urlToRegexp = r.url.replace(/{(\w*)}/g, '(\\w*)');
			urlToRegexp = RegExp("^"+urlToRegexp+"$", 'g');
			let searchMatch = Array.from(search.matchAll(urlToRegexp));

			if(searchMatch.length) {
				let paramValues = searchMatch[0];
				paramValues.shift();
				let paramKeys = routeMatches.map(item => item[1]);

				for (let i = 0; i < paramKeys.length; i++) {
					params[paramKeys[i]] = paramValues[i];
				}
				return true;
			}
			return false;
		});

		if(!route) {
			route = this.routes[0]; // Taking first as default
			history.pushState({}, '', `?${route.url}`);
		}
		route.params = params;
		route.run();
	}

	goTo(routeName, params) {
		let currentRoute = this.routes.find(r => r.name === routeName);

		let url = currentRoute.url;
		for (const match of url.matchAll(/{(.*?)}/g)) {
			url = url.replace(match[0], params[match[1]])
		}

		currentRoute.params = params;

		history.pushState(params, '', `?${url}`);
		currentRoute.run();
	}

	routeChanged(route) {
		this.listeners.routeChange.forEach(callback => {
			callback.call(route);
		});
	}

	on(eventName, callback) {
		this.listeners[eventName].push(callback);
	}
}
