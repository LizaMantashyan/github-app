class UserDetail extends Module{
  userInfo = {};
  userRepos = [];
  publicReposCount = 0;

  reposPerPage = 20;

  constructor(app, container, params) {
    super(app, container, params);
    this.page = 1;
    this.requestDetails();
  }

  render() {
    let content = document.createElement('div');
    content.className = 'user-page';
    content.appendChild(this.createUserInfo());
      
    let topPagination = this.createPangination();
    topPagination ? content.appendChild(topPagination): null;


    content.appendChild(this.createUserReposInfo());
  

    super.renderHtml(content);
  }

  createAvatar() {
    let avatarContainer = document.createElement('div');
    avatarContainer.className = 'user-avatar';
    let avatar = document.createElement('img');

    avatar.setAttribute('src', this.userInfo.avatar_url);
    let username = document.createElement('div');
    username.className = 'username';
    username.innerHTML = this.userInfo.login;
    avatarContainer.appendChild(avatar);
    avatarContainer.appendChild(username);

    return avatarContainer;
  }
  createUserInfo(){
    let userInfoContainer = document.createElement('div');
    userInfoContainer.className = 'user-info-container';
    userInfoContainer.appendChild(this.createAvatar());
    userInfoContainer.appendChild(this.createUserDetails());
    return userInfoContainer;
  }

  createUserDetails() {
    let userInfoContainer = document.createElement('div');
    userInfoContainer.className = 'user-details-container';
    let lastUpdatedDate =(new Date(this.userInfo.updated_at)).toString().slice(0,24);
    let createdAt = (new Date(this.userInfo.created_at)).toString().slice(0,24);
    userInfoContainer.innerHTML = `
      <div><b>GitHub Url:</b> <a href="${this.userInfo.html_url}" target="_blank">${this.userInfo.html_url}</a></div>
      <div><b>Followers:</b>${this.userInfo.followers}</div>
      <div><b>Following:</b>${this.userInfo.following}</div>
      <div><b>Location:</b>${this.userInfo.location||""}</div>
      <div><b>Created at:</b>${createdAt}</div>
      <div><b>Last updated:</b>${lastUpdatedDate}</div>

    `;
    return userInfoContainer;
  }
  createUserReposInfo() {
    let userRepoContainer = document.createElement('div');
    userRepoContainer.className = 'user-repos-list';
    userRepoContainer.innerHTML =  this.userRepos.map(repo =>
      `<div class="user-repo"><b>${repo.full_name}:</b>
          <a href=${repo.html_url} target="_blank">${repo.html_url}</a>
      </div>`).join("");
    return userRepoContainer;
  }
  createPangination(){
    let paginationContainer = document.createElement('div');
    paginationContainer.className = "pagination-container"

    if(this.publicReposCount <= this.reposPerPage){
      return null;
    }

    let prevDisabled = this.page === 1;
    let nextDisabled = Math.floor((this.publicReposCount-1)/this.reposPerPage)+1 === this.page;

    let prevButton = this.createPaginationButton('<----Prev');
    let nextButton = this.createPaginationButton('Next---->');

    prevButton.disabled = prevDisabled;
    nextButton.disabled = nextDisabled;
    prevButton.addEventListener('click',() => this.requestRepos(this.page-1));
    nextButton.addEventListener('click', ()=> this.requestRepos(this.page+1));

    paginationContainer.appendChild(prevButton);
    paginationContainer.appendChild(nextButton);

    return paginationContainer;

  }
  createPaginationButton(text) {
    let button = document.createElement('button');
    button.innerHTML = text;
    return button;
  }




  requestDetails() {
    try {
      fetch( `https://api.github.com/users/${this.params.username}`)
          .then(result => result.json())
          .then(data => {
            this.userInfo = data;
            this.requestRepos();
            this.publicReposCount = data.public_repos;
          });
    }
    catch(err) {
      console.log(err);
    }
  }
  requestRepos(page) {
    page = page || this.page;
    try {
      fetch( `${this.userInfo.repos_url}?page=${page}&per_page=${this.reposPerPage}`)
          .then(result => result.json())
          .then(data => {
            this.page = page;
            this.userRepos = data;
            this.render();
          }).catch((error)=>alert(error));
    }
    catch(err) {
      console.log(err);
    }
  }
}