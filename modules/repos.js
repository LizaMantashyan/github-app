class Repos extends Module{
    searchQuery = '';
    perPage = 20;
    repos=[];
    constructor(app, container, params) {
      super(app, container, params);
      this.page=1;
      this.totalCount = 0;
      this.render();
     }

    render() {
      let content = document.createElement('div');

      let reposContainer = document.createElement('div');
      reposContainer.className = 'repos-list-container';
      let form = this.createSearchForm();
      content.appendChild(form);
      form.addEventListener('submit', this.onSearch.bind(this));

      let topPagination = this.createPangination();
      topPagination ? content.appendChild(topPagination): null;
      content.appendChild(this.createReposList());
    
      let bottomPagination = this.createPangination();
      bottomPagination? content.appendChild(bottomPagination) : null;




        super.renderHtml(content);
    }
    createSearchForm() {
      let form = document.createElement('form');
      let input = document.createElement('input');
      input.setAttribute('placeholder', 'Repo name');
      input.setAttribute('type', 'text');
      input.value = this.searchQuery;
      input.addEventListener('input', e => {
        this.searchQuery = e.target.value;
      });
      form.appendChild(input);
  
      return form;
    }
    onSearch(e) {
      e.preventDefault();
      this.page = 1;
      this.search();
    }
    createReposList() {
      if(!this.repos.length && this.searchQuery){
        let noSuchValues = document.createElement('div');
        noSuchValues.innerHTML = "No repository matching to your searching criteria...";
        return(noSuchValues);
      }
      let userRepoContainer = document.createElement('div');
      userRepoContainer.className = 'user-repos-list';
      userRepoContainer.innerHTML =  this.repos.map(repo =>
        `<div class="user-repo"><b>${repo.name}:</b>
            <a href=${repo.clone_url} target="_blank">${repo.clone_url}</a>
        </div>`).join("");
      return userRepoContainer;
    }
    createPangination(){
      let paginationContainer = document.createElement('div');
      paginationContainer.className = "pagination-container"
  
      if(this.totalCount > this.perPage){
        let prevDisabled = this.page === 1;
        let nextDisabled = Math.floor((this.totalCount-1)/this.perPage) + 1 === this.page;
  
        let prevButton = this.createPaginationButton('<----Prev');
        let nextButton = this.createPaginationButton('Next---->');
  
        prevButton.disabled = prevDisabled;
        nextButton.disabled = nextDisabled;
  
        prevButton.addEventListener('click',() => this.search(this.page-1));
        nextButton.addEventListener('click',() =>this.search( this.page+1));
  
        paginationContainer.appendChild(prevButton);
        paginationContainer.appendChild(nextButton);
      }else{
        return null;
      }
  
      return paginationContainer;
  
    }
    createPaginationButton(text) {
      let button = document.createElement('button');
      button.innerHTML = text;
      return button;
    }
  
  
    
    search(page) {
      page = page || this.page;
      try {
        fetch(`https://api.github.com/search/repositories?q=${this.searchQuery}+in:name&per_page=${this.perPage}&page=${page}`)
        .then(result => result.json())
        .then(data => {
          this.page = page;
          this.repos = data.items;
          this.totalCount = data.total_count          
          this.render();
          })
      }catch(err) {
        console.log(err);
      }
  
    }
}