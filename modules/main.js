class Main extends Module{

    constructor(app, container) {
        super(app, container);
        this.navUl = document.createElement('ul');
        this.navUl.className="nav-ul";
        app.on('routeChange', this.renderNavigation.bind(this));
    }


    render() {
        let container = document.createElement('div');
        let nav = document.createElement('nav');
        nav.className="navigation";      
        
        nav.appendChild(this.navUl);
        let content = document.createElement('div');
        container.appendChild(nav);
        container.appendChild(content);
        
        this.renderNavigation();
        super.renderHtml(container);
    }

    renderNavigation() {
        this.navUl.innerHTML = "";
        let navigationItems = [
            {
                title: 'Users',
                route: 'users'
            },
            {
                title: 'Repos',
                route: 'repos'
            }
        ];

        navigationItems.forEach(item => this.navUl.appendChild(this.createNavItem(item)));
    }

    createNavItem(item) {
        let navItem = document.createElement('li');
        let navLink = document.createElement('div');
        navLink.innerHTML = item.title;
        navLink.className = this.app.activeRoute.name == item.route ? 'active' : '';
        navLink.addEventListener('click',() =>{
            this.app.goTo(item.route, {});
        } );
        navItem.appendChild(navLink);
        return navItem;
    }
}