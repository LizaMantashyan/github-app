class Users extends Module{
  searchQuery = '';
  perPage = 20;

  constructor(app, container, params) {
    super(app, container, params);
    this.userData = [];
    this.totalCount = 0;
    this.page = 1;

    this.render();
  }
  users = [];
 
  render() {
    let content = document.createElement('div');
   
    let form = this.createSearchForm();
    content.appendChild(form);

    let topPagination = this.createPangination();
    topPagination ? content.appendChild(topPagination): null;
    content.appendChild(this.createUsersList());  

    
    let bottomPagination = this.createPangination();
    bottomPagination? content.appendChild(bottomPagination) : null;

    form.addEventListener('submit', this.onSearch.bind(this));

    super.renderHtml(content);
  }
  createUsersList(){
    if(!this.totalCount && this.searchQuery){
      let noSuchValues = document.createElement('div');
      noSuchValues.innerHTML = "No users matching to your searching criteria...";
      return(noSuchValues);
    }
    let userContainer = document.createElement('div');
    userContainer.className = 'user-list-container';
    this.userData.forEach(item => userContainer.appendChild(this.createUserItem(item)));

    return userContainer;  

  }
  createPangination(){
    let paginationContainer = document.createElement('div');
    paginationContainer.className = "pagination-container"

    if(this.totalCount > this.perPage){
      let prevDisabled = this.page === 1;
      let nextDisabled = Math.floor((this.totalCount-1)/this.perPage)+1 === this.page;

      let prevButton = this.createPaginationButton('<----Prev');
      let nextButton = this.createPaginationButton('Next---->');

      prevButton.disabled = prevDisabled;
      nextButton.disabled = nextDisabled;

      prevButton.addEventListener('click',() => this.search(this.page-1));
      nextButton.addEventListener('click',() =>this.search( this.page+1));

      paginationContainer.appendChild(prevButton);
      paginationContainer.appendChild(nextButton);
    }else{
      return null;
    }

    return paginationContainer;

  }

  createSearchForm() {
    let form = document.createElement('form');
    let input = document.createElement('input');
    input.setAttribute('placeholder', 'User name');
    input.setAttribute('type', 'text');
    input.value = this.searchQuery;
    input.addEventListener('input', e => {
      this.searchQuery = e.target.value;
    });
    form.appendChild(input);

    return form;
  }

  createUserItem(item) {
    let container = document.createElement('div');
    let avatar = document.createElement('img');
    container.className = 'user-item';
    let loginContainer = document.createElement('div');
    loginContainer.innerHTML = item.login;
    avatar.setAttribute('src', item.avatar_url);
    container.appendChild(avatar);
    container.appendChild(loginContainer);

    container.addEventListener('click', this.app.goTo.bind(this.app, 'user-detail', {username: item.login}));

    return container;
  }

  createPaginationButton(text) {
    let button = document.createElement('button');
    button.innerHTML = text;
    return button;
  }

  onSearch(e) {
    e.preventDefault();
    this.page = 1;
    this.search();
  }

  search(page) {
    page = page || this.page;
    try {
      fetch( `https://api.github.com/search/users?q=${this.searchQuery}&per_page=${this.perPage}&page=${page}`)
      .then(result => result.json())
      .then(data => {
          this.page = page;
          this.userData = data.items;
          this.totalCount = data.total_count;
          this.render();
        })
    }catch(err) {
      console.log(err);
    }

  }
}