class Module {
    constructor(app, container, params) {
        this.app = app;
        this.params = params;
        this.container = container;
    }

    renderHtml(content) {
        document.getElementById(this.container).innerHTML = '';
        document.getElementById(this.container).appendChild(content);
    }
}